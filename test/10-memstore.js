const M        = require('../');
const test     = require('tape');
const memstore = M.memstore;

test('Memstore - Basic data retention', t => {
  t.plan(5);

  // Main functions
  t.equal(memstore.getItem('pizza')           , undefined, 'memstore.getItem returns undefined before setting');
  t.equal(memstore.setItem('pizza', 'calzone'), undefined, 'memstore.setItem returns undefined');
  t.equal(memstore.getItem('pizza')           , 'calzone', 'memstore.getItem returns what was set');
  t.equal(memstore.removeItem('pizza')        , undefined, 'memstore.removeItem returns undefined');
  t.equal(memstore.getItem('pizza')           , undefined, 'memstore.getItem returns undefined after key deletion');
});
