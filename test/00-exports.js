const M    = require('../');
const test = require('tape');

test('Exports', t => {
  t.plan(6);

  // Main functions (7)
  t.equal(typeof M         , 'object'  , 'Module is an object');
  t.equal(typeof M.default , 'function', '\'default\' is a function');
  t.equal(typeof M.memstore, 'object'  , '\'memstore\' is an object');

  // Memstore
  t.equal(typeof M.memstore.getItem   , 'function', '\'memstore.getItem\' is a function');
  t.equal(typeof M.memstore.setItem   , 'function', '\'memstore.setItem\' is a function');
  t.equal(typeof M.memstore.removeItem, 'function', '\'memstore.removeItem\' is a function');
});
