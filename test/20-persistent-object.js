const M        = require('../');
const test     = require('tape');
const memstore = M.memstore;

test('Persistent-object', t => {
  t.plan(4);

  const obj = M.default({ store: memstore, key: 'test' });
  const dup = M.default({ store: memstore, key: 'test' });
  t.ok(obj === dup, 'Dual reference to the same key returns the same object');
  t.equal(JSON.stringify(obj), '{}', 'New persistent-object looks like an empty object');
  obj.greeting = 'Hello';
  obj.subject  = 'World';
  t.equal(JSON.stringify(obj), '{"greeting":"Hello","subject":"World"}', 'Persistent-object toJSON is functional');
  t.equal(memstore.getItem('test'), '{"greeting":"Hello","subject":"World"}', 'Memstore contains json-encoded object');
});
