import { observer } from '@appvise/observer';

import { memstore as Memstore } from './memstore';
export const memstore = Memstore;

// References, so we return the same objects
// Prevents inconsistent state within thread
const references = new WeakMap();

export type CoreOptions = {
  key: string;
  store: {
    getItem: (key: string) => any;
    setItem: (key: string, value: any) => void;
    removeItem: (key: string) => void;
  }
};

// Main fn
export default function(options: Partial<CoreOptions> = {}) {
  const opts: CoreOptions = Object.assign({
    key  : 'persistent',
    store: memstore,
  }, options);

  // Pre-existing reference
  const refs = references.get(opts.store) || {};
  if (opts.key in refs) {
    return refs[opts.key];
  }

  // Start observing
  const org = JSON.parse(opts.store.getItem(opts.key) || '{}');
  const ref = observer(org, () => {
    opts.store.setItem(opts.key, JSON.stringify(org));
  });

  // Store new reference
  refs[opts.key] = ref;
  references.set(opts.store, refs);

  // Return new reference
  return ref;
}
