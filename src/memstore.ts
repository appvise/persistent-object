// Emulates localstorage interface
export const memstore = Object.create({
  getItem(key: string) {
    return memstore[key];
  },
  setItem(key: string, value: any) {
    memstore[key] = value;
  },
  removeItem(key: string) {
    delete memstore[key];
  }
});
